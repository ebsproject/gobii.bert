package org.gobii.bert;

import ernie.core.Ernie;
import java.io.File;
import org.gobii.bert.util.Assertions;

public class Bert {

	private static Ernie ernie = new Ernie();

	public static Ernie getErnie() {
		return ernie;
	}

	public static void main(String[] args) throws Exception {

		ernie.loadComponents(Components.class);
		ernie.loadComponents(BertUtils.class);
		ernie.loadComponents(Assertions.class);

		for (int i = 0 ; i < args.length ; i++) {
			if ("-c".equals(args[i])) {
				i++;
				System.out.println("Running inline: " + args[i]);
				ernie.runScript(args[i]);
			} else {
				System.out.println("Running file " + args[i]);
				ernie.runFile(args[i]);
			}
		}

		File dir = new File("./bert-results");
		if (! (dir.isDirectory() && dir.exists())) {
			dir.delete();
			dir.mkdir();
		}


		ernie.report();
		ernie.report("./bert-results");

//		Scanner scanner = new Scanner(System.in);
//		while (true) {
//
//			String line = scanner.nextLine();
//			if ("exit".equals(line)) {
//				break;
//			} else {
//				try {
//					System.out.println(ernie.runScript(line));
//				} catch (Throwable e) {
//					e.printStackTrace();
//				}
//			}
//		}

		System.exit(0);
	}
}
