package org.gobii.bert.shell;

public interface Shell {

	void execute(String command) throws Exception;

	void scpRemote(String from, String to) throws Exception;

	void scp(String from, String to) throws Exception;

	void scpDirectory(String directoryPath, String to) throws Exception;

	void scpContent(String content, String to) throws Exception;

	String slurp(String path) throws Exception;
}
