package org.gobii.bert.matrix;

import java.util.Arrays;
import java.util.List;

public class Row<T> extends Matrix<T> implements Vector<T> {
	public Row(int length) {
		super(1, length);
	}

	public Row(T[] ts) {
		this(Arrays.asList(ts));
	}

	public Row(List<T> ts) {
		this(ts.size());
		for (int i = 0 ; i < ts.size() ; i++) {
			this.set(i, ts.get(i));
		}
	}

	@Override
	public T get(int i) {
		return super.get(0, i);
	}

	@Override
	public void set(int i, T t) {
		super.set(0, i, t);
	}

	@Override
	public int length() {
		return super.width();
	}

	public Row<T> slice(int i) {
		Row<T> m = new Row<>(this.length() - i);
		for (int j = 0; j < m.length() ; j++, i++) {
			m.set(j, this.get(i));
		}

		return m;
	}
}
