package org.gobii.bert.matrix;

import java.util.ArrayList;
import java.util.List;

public class Matrix<T> {

	private List<List<T>> data;

	public Matrix(int height, int width) {
		data = new ArrayList<>(height);
		for (int i = 0 ; i < height ; i++) {
			data.add(new ArrayList<T>(width));
			for (int j = 0 ; j < width ; j++) {
				data.get(i).add(null);
			}
		}
	}

	public T get(int i, int j) {
		return data.get(i).get(j);
	}

	public void set(int i, int j, T t) {
		data.get(i).set(j, t);
	}

	private Integer height = null;
	public int height() {
		if (height == null) {
			height = this.data.size();
		}
		return height;
	}

	private Integer width = null;
	public int width() {
		if (width == null) {
			width = this.data.get(0).size();
		}
		return width;
	}

	public Matrix<T> slice(int i, int j, int height, int width) {
		Matrix<T> matrix = new Matrix<>(height, width);
		for (; i < height && i < this.height() ; i++) {
			for (; j < width && j < this.width() ; j++) {
				matrix.set(i, j, this.get(i, j));
			}
		}
		return matrix;
	}

	public Matrix<T> slice(int i, int j) {
		return slice(i, j, height() - i, width() - j);
	}

	public Column<T> getColumn(int j) {
		Column<T> column = new Column<>(this.height());
		for (int i = 0 ; i < height() ; i++) {
			column.set(i, this.get(i, j));
		}
		return column;
	}

	public Row<T> getRow(int i) {
		Row<T> row = new Row<>(this.width());
		for (int j = 0 ; j < width() ; j++) {
			row.set(j, this.get(i, j));
		}
		return row;
	}

	public Matrix<T> transpose() {
		Matrix<T> m = new Matrix<T>(width, height) {
			@Override
			public T get(int i, int j) {
				return data.get(j).get(i);
			}

			@Override
			public void set(int i, int j, T t) {
				data.get(j).set(i, t);
			}
		};

		m.data = this.data;

		return m;
	}
}
