package org.gobii.bert.util;

import ernie.core.Action;
import java.util.Objects;

public class Comparisons {

	@Action("equals")
	public boolean equals(Object x, Object y) {
		return Objects.equals(x, y);
	}

}
